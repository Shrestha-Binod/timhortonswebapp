package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}
	@Test
	public void testDrinksRegular() {
		  MealType type = MealType.DRINKS;
	        List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);	        
			assertTrue("Invalid value for meal", !(types.isEmpty()));
	
	}
	@Test
	public void testDrinksException() {
		  MealType type = MealType.DRINKS;
	        List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);	  
	        assertFalse("Invalid input", (types.get(0) != "No brand available"));	
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		  MealType type = MealType.DRINKS;
	        List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);	        
			assertTrue("Invalid value for mealtype", (types.size()>3));
	
	}
	@Test
	public void testDrinksBoundaryOut() {
		  MealType type = MealType.DRINKS;
	        List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);	        
			assertFalse("Invalid value for mealtype", (types.size()!=1));
	
	}
	
	

}
