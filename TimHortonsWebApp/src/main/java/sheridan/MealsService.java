package sheridan;

import sheridan.MealType;

import java.util.ArrayList;
import java.util.List;

public class MealsService {
	

    public static List<String> getAvailableMealTypes( MealType type ){

        List<String> types = new ArrayList<String>( );

        if( type != null && type.equals( MealType.DRINKS ) ){
//          types.add( "TBD" );
//          types.add( "TBD" ); Coffee, Ice Cap, Tea, Water
        	types.add("Coffee");
        	types.add("Ice Cap");
        	types.add("Tea");
        	types.add("Water");
        }
        else if( type != null && type.equals( MealType.BAKEDGOODS ) ){
//            types.add( "TBD" );
//            types.add( "TBD" );Bagels, Donuts, Muffins, Timbits
        	types.add("Bagels");
        	types.add("Donuts");
        	types.add("Muffins");
        	types.add("Timbits");
        }
        else {
        	types.add("No Brand Available");
        }
        return types;
    }
}
